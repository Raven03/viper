#!/usr/bin/env python


class ScoreView:

    def print_score_details(self, board):
        board.itemconfigure(board.find_withtag("score"), text=f"Score: {board.score.score}")
