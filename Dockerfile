FROM python:3.7-buster

RUN useradd -ms /bin/bash snake_user
WORKDIR /home/snake_user

COPY dist/main /home/snake_user/main

RUN chmod -R 777 /home/snake_user/
USER snake_user
CMD ./main
