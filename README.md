# Viper
## Developer setup for linux system with anaconda3(short: conda)

- [ ] Download conda:
```
$ cd /tmp
$ curl -O https://repo.anaconda.com/archive/Anaconda3-2019.03-Linux-x86_64.sh
```

- [ ] Verify, if the downlaod is correct
    > (Checksumme: 45c851b7497cc14d5ca060064394569f724b67d9b5f98a926ed49b834a6bb73a  Anaconda3-2019.03-Linux-x86_64.sh): <br>
```
$ sha256sum Anaconda3-2019.03-Linux-x86_64.sh
```
- [ ] Install conda:
    * Press **Return** once.
    * Enter **yes**, to be able to use **conda** in your terminal.
```
$ bash Anaconda3-2019.03-Linux-x86_64.sh
```

- [ ] Activate conda: <br>
```
$ source ~/.bashrc
```

- [ ] Verify, if the installation was successful: <br>
```
$ conda list
```

- [ ] In the following we use a virtual environment called **pyGameEnv** with **Python version 3.7**:
    * If possible skip this step, because the IDE PyCharm builds conda environment itself:
```
$ conda env create -f environment.yml
```
<br>
---
<br>

Return to normal from here:

To use the created environment we execute the following command: <br>
```
$ conda activate Viper
```

Installation of the needed packages in conda: <br>
```
$ conda install -c <location_file_package>
```

Installation of the needed packages in the virtual conda environment (using pip): <br>
```
$ ~/anaconda3/envs/<your_env_name>/bin/pip install -r requirements.txt
```

Compile the project with the following line in the temrinal: <br>
```
$ pyinstaller --name main --onefile main.py
```

## Play the game

A playable version of the snake game [here](https://www.dropbox.com/s/uxfslpbmslmt27d/viper.zip?dl=0).

### Without Docker and docker-compose
Because of the game was compiled on a ubuntu system, it's only playable direct on the operating system if you are using
a ubuntu system with the version 19.04 or higher. Further you have to run a mysql database on your system on port 3306 
and run the script wiggle_snake_on_ubuntu.sh to start the game directly on your host system.

###With Docker and docker-compose
Another approach to play our game is to install the tool [docker](https://www.docker.com/get-started) and 
[docker-compose](https://docs.docker.com/compose/install/#install-compose) on your host system and launch the 
docker-compose.yml. Everything is built up for you and the fun can begin!



