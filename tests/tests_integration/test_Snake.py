#! /usr/bin/env python
import os
import unittest

from sqlalchemy import create_engine
from sqlalchemy.exc import OperationalError

from snake import *
from pyvirtualdisplay import Display

SNAKE_MYSQL_USER = os.environ.get('PYTHON_MYSQL_USER')
HOST = os.environ.get('PYTHON_MYSQL_HOST')


class IntegrationTestSnake(unittest.TestCase):
    def setUp(self):
        self.display = Display(visible=0, size=(1366, 768))
        self.display.start()
        if not os.environ.get('PYTHON_MYSQL_HOST') == 'mysql':
            os.environ['PYTHON_MYSQL_HOST'] = '127.0.0.1'
        self.snake = Snake()

    def tearDown(self):
        self.snake.game_over()
        self.display.stop()

    def test_game_is_connected_to_database(self):
        engine = create_engine("mysql://" + str(SNAKE_MYSQL_USER) + ":snake1234@" + HOST + "/snake_mysql", echo=True)
        database_is_connected = True
        try:
            engine.connect()
        except OperationalError:
            database_is_connected = False

        self.assertTrue(database_is_connected)
