#! /usr/bin/env python
import os
import unittest

from pyvirtualdisplay import Display
from sqlalchemy import create_engine

from snake import *

SNAKE_TEST_USER = os.environ.get('PYTHON_MYSQL_USER')
HOST = os.environ.get('PYTHON_MYSQL_HOST')

engine = create_engine("mysql://" + str(SNAKE_TEST_USER) + ":snake1234@" + str(HOST) + "/snake_mysql", echo=True)


class IntegrationTestScoreController(unittest.TestCase):

    def setUp(self):
        self.display = Display(visible=0, size=(1366, 768))
        self.display.start()
        if not HOST == 'mysql':
            os.environ['PYTHON_MYSQL_HOST'] = '127.0.0.1'
        self.snake = Snake(str(SNAKE_TEST_USER))
        if HOST == 'mysql':
            Score.__table__.create(bind=engine, checkfirst=True)

    def tearDown(self):
        self.snake.score_controller.delete_current_user_from_database_table_scores()
        self.snake.game_over()
        self.display.stop()

    def test_save_score_in_database(self):
        score_view = ScoreView()
        score = Score(SNAKE_TEST_USER, 2)
        score_controller = ScoreController(score, score_view)

        score_controller.save()
        score_of_test_user = self.snake.score_controller.get_score_by_current_user()

        self.assertEqual(1, len(score_of_test_user))
        self.assertEqual(2, score_of_test_user[0].score)
