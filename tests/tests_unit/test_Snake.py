#! /usr/bin/env python
import unittest
from snake import *
from pyvirtualdisplay import Display


class UnitTestApp(unittest.TestCase):

    def setUp(self):
        self.display = Display(visible=0, size=(1366, 768))
        self.display.start()
        self.snake = Snake()

    def tearDown(self):
        self.snake.game_over()
        self.display.stop()

    def test_snake_initialized_correctly(self):
        self.assertEqual(self.snake.direction_x, 1)
        self.assertEqual(self.snake.direction_y, 0)
        self.assertEqual(self.snake.coordinates, [(100, 100), (80, 100), (60, 100)])
        self.assertIsNotNone(self.snake.snake)
        self.assertIsNotNone(self.snake.food)
        self.assertEqual(self.snake.score.score, 0)

    def test_ate_food_score_rises(self):
        self.snake.coords(self.snake.snake[0], self.snake.coords(self.snake.food))

        self.snake.ate_food()

        self.assertTrue(1, self.snake.score.get_score())

    def test_ate_food_new_food_generated(self):
        old_food_coordinates = self.snake.coords(self.snake.food)
        self.snake.coords(self.snake.snake[0], self.snake.coords(self.snake.food))

        self.snake.ate_food()

        new_food_coordinates = self.snake.coords(self.snake.food)

        self.assertNotEqual(old_food_coordinates, new_food_coordinates)

    def test_ate_food_snake_has_grown(self):
        old_snake_length = len(self.snake.snake)
        self.snake.coords(self.snake.snake[0], self.snake.coords(self.snake.food))

        self.snake.ate_food()

        new_snake_length = len(self.snake.snake)

        self.assertNotEqual(old_snake_length, new_snake_length)

    def test_create_new_food(self):
        old_food_coordinates = self.snake.coords(self.snake.food)

        self.snake.create_new_food()

        new_food_coordinates = self.snake.coords(self.snake.food)

        self.assertNotEqual(old_food_coordinates, new_food_coordinates)


if __name__ == '__main__':
    unittest.main()
