#! /usr/bin/env python
import mysql.connector

if __name__ == "__main__":
    my_db_snake_mysql = mysql.connector.connect(
        host="mysql",
        user="root",
        passwd="snake1234",
        database="snake_mysql"
    )

    my_snake_mysql_cursor = my_db_snake_mysql.cursor()

    my_snake_mysql_cursor.execute("ALTER USER 'TestSnake'@'%' IDENTIFIED WITH mysql_native_password BY 'snake1234';")
