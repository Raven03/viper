#!/usr/bin/env python

import os

from sqlalchemy import create_engine, func
from sqlalchemy.orm import sessionmaker

from domain.Score import Score
from view.ScoreView import ScoreView

HOST = os.environ.get('PYTHON_MYSQL_HOST')
PYTHON_MYSQL_USER = os.environ.get('PYTHON_MYSQL_USER')

engine = create_engine("mysql://" + str(PYTHON_MYSQL_USER) + ":snake1234@" + str(HOST) + "/snake_mysql", echo=True)


class ScoreController:

    def __init__(self, score_model: Score, score_view: ScoreView):
        self.score_model = score_model
        self.score_view = score_view

    def get_score_by_current_user(self):
        session = load_session()

        got_entry = session.query(Score).filter(Score.username.like(self.score_model.username))
        return got_entry.all()

    def delete_current_user_from_database_table_scores(self):
        session = load_session()

        session.query(Score).filter(Score.username.like(self.score_model.username)).delete(synchronize_session='fetch')
        session.commit()

    def updateView(self, board):
        self.score_view.print_score_details(board)

    def set_score_value(self, new_value):
        self.score_model.set_score(new_value)

    def save(self):
        session = load_session()

        new_score = Score(username=self.score_model.username, score=self.score_model.get_score(), timestamp=func.now())

        session.add(new_score)
        session.commit()


def load_session():
    Session = sessionmaker(bind=engine)
    session = Session()
    return session
