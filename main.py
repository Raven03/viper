#! /usr/bin/env python
import os
import tkinter as tk
from snake import Snake

if os.environ.get('DISPLAY', '') == '':
    print('no display found. Using :0.0')
    os.environ.__setitem__('DISPLAY', ':0.0')

if __name__ == "__main__":
    root = tk.Tk()
    root.title("Snake")
    root.resizable(False, False)

    board = Snake()
    board.pack()
    while True:

        root.update()
        root.update_idletasks()

        if not board.game_is_running:
            board.score_controller.save()
            break



