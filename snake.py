#! /usr/bin/env python
import os
import random
import sys
import time
import tkinter as tk
from functools import partial
from tkinter import messagebox

from controller.ScoreController import ScoreController
from domain.Score import Score
from view.ScoreView import ScoreView

BLOCK_SIZE = 20
CANVAS_HEIGHT = 620
CANVAS_WIDTH = 600
BORDER_WIDTH = 20
BORDER_HEIGHT = 20
BOARD_WIDTH = CANVAS_WIDTH - BORDER_WIDTH
BOARD_HEIGHT = CANVAS_HEIGHT - BORDER_HEIGHT
GAME_SPEED = 100 / 1000
MOVE_INCREMENT = 20
FOOD_COLOR = "red"
SNAKE_COLOR = "blue"
TEXT_SCORE_X = BORDER_HEIGHT + 8
TEXT_SCORE_Y = BORDER_WIDTH + 40


class Snake(tk.Canvas):
    def __init__(self, username="User1"):
        super().__init__(width=CANVAS_WIDTH, height=CANVAS_HEIGHT, background="black", highlightthickness=0)

        self.direction_x = 1
        self.direction_y = 0
        self.coordinates = [(100, 100), (80, 100), (60, 100)]

        self.snake, self.food, self.score = self.initialize_board(username)

        if not os.environ.get('PYTHON_TEST_MODE') == "enable":
            # start login dialog:
            self.login_user()
            # end login dialog

        self.score_view = ScoreView()
        self.score_controller = ScoreController(self.score, self.score_view)

        self.game_is_running = True

    def login_user(self):
        beginRound = tk.messagebox.askquestion("WELCOME", "Begin your round?", type="yesno")

        if beginRound == 'yes':
            self.open_login_window()
            print('start game')
        else:
            self.game_is_running = False

    def save_user(self, username, login_window):
        print(self.score.get_username())
        print(username.get())
        self.score.set_username(username.get())
        login_window.destroy()

    def open_login_window(self):
        loginWindow = tk.Tk()
        loginWindow.geometry("450x300")
        loginWindow.title("Ready to beat your highscore?!")
        loginWindow.configure(bg="#052600")
        userNameLabel = tk.Label(loginWindow, text="What's your snake called?", height=2).place(x="150", y="50")
        username = tk.StringVar()
        userNameEntry = tk.Entry(loginWindow, textvariable=username).place(x="165", y="100")

        save_user = partial(self.save_user, username, loginWindow)

        startButton = tk.Button(loginWindow, text="Let's go!", command=save_user).place(x="190", y="180")

        self.game_is_running = True

    def perform_action(self):
        if self.is_collided():
            self.game_over()
            return
        self.ate_food()

    def initialize_board(self, username):
        snake = [
            self.create_rectangle(self.coordinates[0], self.coordinates[0][0] + BLOCK_SIZE,
                                  self.coordinates[0][1] + BLOCK_SIZE,
                                  fill=SNAKE_COLOR),
            self.create_rectangle(self.coordinates[1], self.coordinates[1][0] + BLOCK_SIZE,
                                  self.coordinates[1][1] + BLOCK_SIZE,
                                  fill=SNAKE_COLOR),
            self.create_rectangle(self.coordinates[2], self.coordinates[2][0] + BLOCK_SIZE,
                                  self.coordinates[2][1] + BLOCK_SIZE,
                                  fill=SNAKE_COLOR)]
        food = self.create_rectangle(200, 200, 200 + BLOCK_SIZE, 200 + BLOCK_SIZE, fill=FOOD_COLOR)
        score = Score(username, 0)
        self.create_text(TEXT_SCORE_Y, TEXT_SCORE_X, text=f"Score: {score.score}", tag="score", fill="#fff", font=10)
        self.create_rectangle(BORDER_WIDTH, TEXT_SCORE_Y - BLOCK_SIZE, BOARD_WIDTH, BOARD_HEIGHT, outline="#525d69")
        self.bind_all("<Key>", self.on_key_press)
        self.pack()
        return snake, food, score

    def create_new_food(self):
        food_position_x = random.randrange(BORDER_WIDTH, BOARD_HEIGHT - BLOCK_SIZE, MOVE_INCREMENT)
        food_position_y = random.randrange(TEXT_SCORE_Y, BOARD_HEIGHT - BLOCK_SIZE, MOVE_INCREMENT)
        self.food = self.create_rectangle(food_position_x, food_position_y,
                                          food_position_x + BLOCK_SIZE,
                                          food_position_y + BLOCK_SIZE,
                                          fill=FOOD_COLOR)
        self.pack()

    def ate_food(self):
        if self.coords(self.food) == self.coords(self.snake[0]):
            self.score_controller.set_score_value(self.score.score + 1)
            self.score_controller.updateView(self)
            self.coordinates.append((self.coordinates[-1][0] - (self.coordinates[-2][0] - self.coordinates[-1][0]),
                                     self.coordinates[-1][1] - (self.coordinates[-2][1] - self.coordinates[-1][1])))
            self.snake.append(self.create_rectangle(self.coordinates[-1], self.coordinates[-1][0] + BLOCK_SIZE,
                                                    self.coordinates[-1][1] + BLOCK_SIZE, fill=SNAKE_COLOR))
            self.delete(self.food)
            self.create_new_food()
            self.pack()

    def is_collided(self):
        snake_head_position_x, snake_head_position_y = self.coordinates[0]
        return snake_head_position_x < BORDER_WIDTH \
               or snake_head_position_x > BOARD_WIDTH - BLOCK_SIZE \
               or snake_head_position_y < TEXT_SCORE_Y - BLOCK_SIZE \
               or snake_head_position_y > BOARD_HEIGHT - BLOCK_SIZE \
               or self.coordinates[0] == self.coordinates[2]

    def move_snake(self):
        while self.game_is_running:
            self.coordinates = [(self.coordinates[0][0] + self.direction_x,
                                 self.coordinates[0][1] + self.direction_y)] + self.coordinates[:-1]

            if not self.is_collided():
                self.move(self.snake[0], self.direction_x, self.direction_y)
                for snake_part, snake_part_coordinate in zip(self.snake, self.coordinates):
                    snake_part_coordinate_x, snake_part_coordinate_y = snake_part_coordinate
                    self.coords(snake_part, snake_part_coordinate_x, snake_part_coordinate_y,
                                snake_part_coordinate_x + BLOCK_SIZE, snake_part_coordinate_y + BLOCK_SIZE)
                self.update()
                self.ate_food()
                time.sleep(GAME_SPEED)
            else:
                self.game_over()

    def on_key_press(self, event):
        if event.keysym == "Left" or event.keysym == "a":
            self.direction_x = -MOVE_INCREMENT
            self.direction_y = 0
            self.move_snake()
        elif event.keysym == "Right" or event.keysym == "d":
            self.direction_x = MOVE_INCREMENT
            self.direction_y = 0
            self.move_snake()
        elif event.keysym == "Up" or event.keysym == "w":
            self.direction_x = 0
            self.direction_y = -MOVE_INCREMENT
            self.move_snake()
        elif event.keysym == "Down" or event.keysym == "s":
            self.direction_x = 0
            self.direction_y = MOVE_INCREMENT
            self.move_snake()
        elif event.keysym == "Escape":
            self.game_over()

    def reset_game(self):
        python = sys.executable
        os.execl(python, python, *sys.argv)

    def game_over(self):
        if not os.environ.get('PYTHON_TEST_MODE') == "enable":
            tryAgain = tk.messagebox.askyesno("GAME OVER", "Would you like to try again?")
            print("try again", tryAgain)
        else:
            tryAgain = False

        if tryAgain:
            self.score_controller.save()
            self.reset_game()
        else:
            self.game_is_running = False
