#!/usr/bin/env python

import os

from sqlalchemy import *
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

HOST = os.environ.get('PYTHON_MYSQL_HOST')
PYTHON_MYSQL_USER = os.environ.get('PYTHON_MYSQL_USER')

engine = create_engine("mysql://" + str(PYTHON_MYSQL_USER) + ":snake1234@" + str(HOST) + "/snake_mysql", echo=True)
Base = declarative_base()


class Score(Base):
    __tablename__ = 'Scores'

    id = Column(Integer, autoincrement=True, nullable=False, primary_key=True)
    timestamp = Column(TIMESTAMP, nullable=True, onupdate=func.now())
    username = Column(VARCHAR(255), nullable=False)
    score = Column(Integer, nullable=False)

    def __init__(self, username="snake_user", score=0, timestamp="0000-00-00 00:00:00"):
        Score.__table__.create(bind=engine, checkfirst=True)
        self.username = username
        self.score = score
        self.timestamp = timestamp

    def set_score(self, score):
        self.score = score

    def set_username(self, username):
        self.username = username

    def get_username(self):
        return self.username

    def get_score(self):
        return self.score
